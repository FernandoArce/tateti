package Negocio;

public class Jugador {
	String nombre;
	int puntaje;
	
	public Jugador() {
		this.nombre=getNombre();
		this.puntaje=0;		
	}	
	public String getNombre() {
		return nombre;
	}
	public int getPuntaje() {
		return puntaje;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}
	public void incrementarPuntaje() {
		this.puntaje++;
	}
}