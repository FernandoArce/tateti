package Negocio;

public class Tablero {
	int tablero[];

	 public Tablero() {
		 int tab[]=  {13,23,33,43,53,63,73,83,93};
		 this.tablero=tab;
	 }
	 public void reiniciar() {		
		 for (int i=0; i<9;i++) {		
				tablero[i]=i+2;
				}
		 }

	public void marcar(int jugador, int i) {
		 tablero[i]=jugador;
	}

	public int[] getTablero() {
		return this.tablero;
	}

	public int getValor(int i) {
		return tablero[i];
	}
	
}
